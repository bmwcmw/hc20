import static java.util.Arrays.asList;
import static java.util.Comparator.reverseOrder;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Application {

    private static final String SA = "a_example.txt";
    private static final String SB = "b_read_on.txt";
    private static final String SC = "c_incunabula.txt";
    private static final String SD = "d_tough_choices.txt";
    private static final String SE = "e_so_many_books.txt";
    private static final String SF = "f_libraries_of_the_world.txt";

    public static void main(String[] args) throws IOException {
        String file = SF;
        Path path = Paths.get("/home/aboivin/workspace/hc20/files_in/" + file);
        List<String> lines = Files.lines(path).collect(toList());
        String[] first = lines.get(0).split(" ");
        int B = Integer.valueOf(first[0]);
        int L = Integer.valueOf(first[1]);
        int D = Integer.valueOf(first[2]);

        int[] scores = Arrays.stream(lines.get(1).split(" ")).mapToInt(Integer::valueOf).toArray();
        int[] bookNumber = new int[L];
        int[] signUp = new int[L];
        int[] maxBookPerDay = new int[L];
        Map<Integer, List<Integer>> books = new HashMap<>();

        for (int i = 2; i < lines.size(); i++) {
            int index = (i - 2) / 2;
            String line = lines.get(i);
            if(i % 2 == 0) {
                String[] sp = line.split(" ");
                if(sp.length > 0 && !sp[0].isEmpty()) {
                    bookNumber[index] = Integer.valueOf(sp[0]);
                    signUp[index] = Integer.valueOf(sp[1]);
                    maxBookPerDay[index] = Integer.valueOf(sp[2]);
                }
            } else {
                List<Integer> sp = Arrays.stream(line.split(" ")).map(Integer::valueOf).sorted((b1,b2) -> Integer.compare(scores[b2], scores[b1])).collect(toList());
                books.put(index, sp);
            }
        }
        System.out.println("Loaded.");
//        infos(B, L, D, scores, bookNumber, signUp, maxBookPerDay, books);

//        List<Integer> libraries = asList(1, 0);
//        List<List<Integer>> submission = asList(asList(5, 2, 3), asList(0, 1, 2, 3, 4));
//        System.out.println("Score " + Scoring.score(submission, libraries, signUp, maxBookPerDay, scores, D));

        Lib[] libraryScore = new Lib[L];
        for (int i = 0; i < L; i++) {
            int sum = books.get(i).parallelStream().mapToInt(a -> scores[a]).sum();
            libraryScore[i] = new Lib(i, sum);
        }

        List<Integer> libraries = new ArrayList<>();
        List<List<Integer>> submission = new ArrayList<>();
        Arrays.sort(libraryScore);
        Set<Integer> alreadySubmited = new HashSet<>();
        for (int i = 0; i < libraryScore.length; i++) {
            libraries.add(libraryScore[i].id);
            List<Integer> sub = books.get(libraryScore[i].id);
            sub.removeAll(alreadySubmited);
            submission.add(sub);
            alreadySubmited.addAll(sub);
        }

        Output.output(submission, libraries, file);
    }

    static class Lib implements Comparable {
        int id;
        long score;

        public Lib(int id, long score) {
            this.id = id;
            this.score = score;
        }

        @Override
        public int compareTo(Object o) {
            Lib that = (Lib) o;
            return Long.compare(that.score, this.score);
        }
    }

    private static void infos(int b, int l, int d, int[] scores, int[] bookNumber, int[] signUp, int[] maxBookPerDay, Map<Integer, Set<Integer>> books) {
        System.out.println(Arrays.toString(scores));
        System.out.println(Arrays.toString(bookNumber));
        System.out.println(Arrays.toString(maxBookPerDay));
        System.out.println(Arrays.toString(signUp));
        books.forEach((k,v) -> {
            System.out.println(k + " [" + v.toString() + "]");
        });
        System.out.println(b + " " + l +  " " + d);
        System.out.println("Sum " + Arrays.stream(scores).sum());
    }
}

/*
6 2 7
1 2 3 6 5 4
5 2 2
0 1 2 3 4
4 3 1
3 2 5 0
There are 6 books, 2 libraries, and 7 days for scanning.
The scores of the books are 1, 2, 3, 6, 5, 4 (in order).
Library 0 has 5 books, the signup process takes 2 days, and the library
can ship 2 books per day.
The books in library 0 are: book 0, book 1, book 2, book 3, and book 4.
Library 1 has 4 books, the signup process takes 3 days, and the library can
ship 1 book per day.
The books in library 1 are: book 3, book 2, book 5 and book 0.
 */
