import entity.BookLibInfo;
import entity.Submission;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Application2 {

    private static final String FILE1 = "files_in/a_example.txt";
    private static final String FILE2 = "files_in/b_read_on.txt";
    private static final String FILE3 = "files_in/c_incunabula.txt";
    private static final String FILE4 = "files_in/d_tough_choices.txt";
    private static final String FILE5 = "files_in/e_so_many_books.txt";
    private static final String FILE6 = "files_in/f_libraries_of_the_world.txt";

    public static void main(String[] args) throws IOException {
        String[] files = new String[]{FILE1, FILE2, FILE3, FILE4, FILE5};
        for (String file: files){
            BookLibInfo bookLibInfo = Reader.readFromFile(file);
            Submission submission = LibrarySorterByMaxCanSubmitScore.sortLibrary(bookLibInfo);
            output(file.replace("files_in", "files_out"), submission.submissions, submission.libraries);
        }
    }

    public static int sum(Set<Integer> list) {
        int sum = 0;

        for (int i : list)
            sum = sum + i;

        return sum;
    }

    public static void output(String pathStr, List<List<Integer>> submission, List<Integer> libraries) throws IOException {
        Path path = Paths.get(pathStr);

        String content = "" + submission.size() + '\n';

        for (int i = 0; i < libraries.size(); i++) {
            content += libraries.get(i) + " " + submission.get(i).size();
            content += '\n';
            content += submission.get(i).stream().map(String::valueOf).collect(Collectors.joining(" "));
            content += '\n';
        }
        Files.write(path, content.getBytes());
    }
}

/*
6 2 7
1 2 3 6 5 4
5 2 2
0 1 2 3 4
4 3 1
3 2 5 0
There are 6 books, 2 libraries, and 7 days for scanning.
The scores of the books are 1, 2, 3, 6, 5, 4 (in order).
Library 0 has 5 books, the signup process takes 2 days, and the library
can ship 2 books per day.
The books in library 0 are: book 0, book 1, book 2, book 3, and book 4.
Library 1 has 4 books, the signup process takes 3 days, and the library can
ship 1 book per day.
The books in library 1 are: book 3, book 2, book 5 and book 0.
 */
