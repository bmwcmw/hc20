import entity.BookLibInfo;
import entity.Submission;

import java.util.*;

public class LibrarySorterByMaxCanSubmitScore {
    public static Submission sortLibrary(BookLibInfo bookLibInfo) {
        List<Map.Entry<Integer, Set<Integer>>> entryList = new ArrayList<>(bookLibInfo.booksByLibrary.entrySet());
        for (Map.Entry<Integer, Set<Integer>> entry : entryList) {
            int libId = entry.getKey();
            System.out.println("for lib "+ libId);
            Set<Integer> bookSet = entry.getValue();
            List<Integer> bookList = new ArrayList<>(bookSet);
            bookList.sort(Comparator.comparingInt(bookId -> bookLibInfo.scores[bookId])); // min to max
            System.out.println("who has books " + Arrays.toString(bookList.toArray()));

            int canSubmitDays = bookLibInfo.duration - bookLibInfo.signUp[libId];
            System.out.println("canSubmitDays " + canSubmitDays);

            int canSubmitBooks = bookLibInfo.maxBookPerDay[libId];
            System.out.println("canSubmitBooks " + canSubmitBooks);

            int totalSubmitBooks = canSubmitDays * canSubmitBooks;
            System.out.println("totalSubmitBooks " + totalSubmitBooks);

            List<Integer> toSubmitBooks = new ArrayList<>();
            int maxScore = 0;
            for (int i = 0; i < bookList.size() && i < totalSubmitBooks; i++) {
                int bookId = bookList.get(bookList.size() - 1 - i);
                toSubmitBooks.add(bookId);
                maxScore += bookLibInfo.scores[bookId];
            }
            System.out.println("toSubmitBooks " + Arrays.toString(toSubmitBooks.toArray()));

            bookLibInfo.maximumScorePerLibrary.put(libId, maxScore);
            bookLibInfo.submissionPerLibrary.put(libId, toSubmitBooks);
            System.out.println("_____________------------------________________");
        }

        entryList.sort(Comparator.comparingInt(entry -> {
            int libId = entry.getKey();
            return -bookLibInfo.maximumScorePerLibrary.get(libId);
        }));
        // sorted by max scores

        // refine with already seen books
        Set<Integer> alreadySeenBooks = new HashSet<>();
        for (Map.Entry<Integer, Set<Integer>> entry : entryList) {
            int libId = entry.getKey();
            System.out.println("2nd turn for lib " + libId);

            List<Integer> toSubmit = bookLibInfo.submissionPerLibrary.get(libId);
            if (toSubmit == null || toSubmit.size() == 0) {
                continue;
            }
            System.out.println("2nd turn before toSubmit " + Arrays.toString(toSubmit.toArray()));

            Set<Integer> bookSet = entry.getValue();
            List<Integer> bookList = new ArrayList<>(bookSet);
            bookList.sort(Comparator.comparingInt(bookId -> bookLibInfo.scores[bookId])); // min to max

            int toSubmitSize = toSubmit.size();
            int replaceBookCandidateId = toSubmitSize;
            for (int i = 0; i < toSubmitSize; i++) {
                Integer bookId = toSubmit.get(i);
                if (alreadySeenBooks.contains(bookId)) {
                    if (replaceBookCandidateId == bookList.size()) {
                        break;
                    }
                    Integer newBookId = bookList.get(replaceBookCandidateId);
                    replaceBookCandidateId++;
                    while (replaceBookCandidateId < bookList.size() && alreadySeenBooks.contains(newBookId)) {
                        newBookId = bookList.get(replaceBookCandidateId);
                        replaceBookCandidateId++;
                    }
                    if (!alreadySeenBooks.contains(newBookId)) {
                        toSubmit.set(i, newBookId);
                    }
                } else {
                    alreadySeenBooks.add(bookId);
                }
            }

            System.out.println("2nd turn after toSubmit " + Arrays.toString(toSubmit.toArray()));
        }

        List<Integer> libraries = new ArrayList<>();
        List<List<Integer>> submissions = new ArrayList<>();
        for (Map.Entry<Integer, Set<Integer>> entry : entryList) {
            List<Integer> toSubmit = bookLibInfo.submissionPerLibrary.get(entry.getKey());
            if (toSubmit != null && toSubmit.size() > 0) {
                libraries.add(entry.getKey());
                submissions.add(toSubmit);
            }
        }
        Submission submission = new Submission();
        submission.submissions = submissions;
        submission.libraries = libraries;
        return submission;
    }

    public static int sum(int[] scores, Set<Integer> list) {
        int sum = 0;

        for (int i : list)
            sum = sum + scores[i];

        return sum;
    }

}
