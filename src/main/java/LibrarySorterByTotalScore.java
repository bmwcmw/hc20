import java.util.*;

public class LibrarySorterByTotalScore {
    public static Map<Integer, Set<Integer>> sortLibraryByTotalScore(int[] scores, Map<Integer, Set<Integer>> books) {
        List<Map.Entry<Integer, Set<Integer>>> entryList = new ArrayList<>(books.entrySet());
        entryList.sort(Comparator.comparingInt(entry -> sum(scores, entry.getValue())));

        Map<Integer, Set<Integer>> result = new LinkedHashMap<>();
        for (Map.Entry<Integer, Set<Integer>> entry : entryList) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }

    public static int sum(int[] scores, Set<Integer> list) {
        int sum = 0;

        for (int i : list)
            sum = sum + scores[i];

        return sum;
    }

}
