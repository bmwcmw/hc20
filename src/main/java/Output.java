import static java.util.Arrays.asList;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class Output {

    public static void output(List<List<Integer>> submission, List<Integer> libraries, String name) throws IOException {
        Path path = Paths.get("/home/aboivin/workspace/hc20/files_out/" + name);

        String content = "";

        int count = 0;
        for (int i = 0; i < libraries.size(); i++) {
            if(submission.get(i).size() > 0) {
                count++;
                content += libraries.get(i) + " " + submission.get(i).size();
                content += '\n';
                content += submission.get(i).stream().map(String::valueOf).collect(Collectors.joining(" "));
                content += '\n';
            }
        }

        content = "" + count + '\n' + content;
        Files.write(path, content.getBytes());
    }

    public static void main(String[] args) throws IOException {
        List<Integer> libraries = asList(1, 0);
        List<List<Integer>> submission = asList(asList(5, 2, 3), asList(0, 1, 2, 3, 4));
        Output.output(submission, libraries, " ");
    }
}
