import entity.BookLibInfo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

public class Reader {
    public static BookLibInfo readFromFile(String file) throws IOException {
        Path path = Paths.get(file);
        List<String> lines = Files.lines(path).collect(toList());
        String[] first = lines.get(0).split(" ");
        int B = Integer.valueOf(first[0]);
        int L = Integer.valueOf(first[1]);
        int D = Integer.valueOf(first[2]);

        int[] scores = Arrays.stream(lines.get(1).split(" ")).mapToInt(Integer::valueOf).toArray();
        int[] bookNumber = new int[L];
        int[] signUp = new int[L];
        int[] maxBookPerDay = new int[L];
        Map<Integer, Set<Integer>> books = new HashMap<>();

        for (int i = 2; i < lines.size(); i++) {
            int index = (i - 2) / 2;
            String line = lines.get(i);
            if(i % 2 == 0) {
                String[] sp = line.split(" ");
                if(sp.length > 0 && !sp[0].isEmpty()) {
                    bookNumber[index] = Integer.valueOf(sp[0]);
                    signUp[index] = Integer.valueOf(sp[1]);
                    maxBookPerDay[index] = Integer.valueOf(sp[2]);
                }
            } else {
                Set<Integer> sp = Arrays.stream(line.split(" ")).map(Integer::valueOf).collect(toSet());
                books.put(index, sp);
            }
        }

        BookLibInfo bookLibInfo = new BookLibInfo();
        System.out.println("duration:" + D);
        bookLibInfo.duration = D;
        System.out.println("scores:" + Arrays.toString(scores));
        bookLibInfo.scores = scores;
        System.out.println("bookNumber:" + Arrays.toString(bookNumber));
        bookLibInfo.bookNumber = bookNumber;
        System.out.println("maxBookPerDay:" + Arrays.toString(maxBookPerDay));
        bookLibInfo.maxBookPerDay = maxBookPerDay;
        System.out.println("signUp:" + Arrays.toString(signUp));
        bookLibInfo.signUp = signUp;
        books.forEach((k,v) -> {
            System.out.println("library:" + k + " [" + v.toString() + "]");
        });
        bookLibInfo.booksByLibrary = books;

        System.out.println("----------------Parsing Done------------------");
        return bookLibInfo;
    }
}
