import static java.util.Arrays.asList;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Scoring {

    static int score(List<List<Integer>> submission,
                     List<Integer> libraries,
                     int[] signUp,
                     int[] maxBookPerDay,
                     int[] scores,
                     int D) {
        int day = 0;
        Set<Integer> scoredBook = new HashSet<>();
        for (int i = 0; i < libraries.size(); i++) {
            System.out.println(day);
            int s = signUp[libraries.get(i)];
            day += s;
            System.out.println("day " + day);
            List<Integer> books = submission.get(i);
            for(int d = 0; d < books.size() ; d+=maxBookPerDay[libraries.get(i)]) {
                System.out.println("d " + d + " " + books.size() % maxBookPerDay[libraries.get(i)]);
                if(day + d + 1 <= D) {
                    for (int j = d*maxBookPerDay[libraries.get(i)]; j <= d*maxBookPerDay[libraries.get(i)] + maxBookPerDay[libraries.get(i)]; j++) {
                        if(books.size() > j) {
                            System.out.println("Adding " + books.get(j));
                            scoredBook.add(books.get(j));
                        }
                    }
                }
            }
        }

        return scoredBook.stream().mapToInt(b -> scores[b]).sum();
    }
}
