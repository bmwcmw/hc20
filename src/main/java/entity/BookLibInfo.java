package entity;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class BookLibInfo {
    public int[] scores;
    public int[] bookNumber;
    public int[] maxBookPerDay;
    public int[] signUp;
    public Map<Integer, Set<Integer>> booksByLibrary;
    public int duration;

    public Map<Integer, Integer> maximumScorePerLibrary = new HashMap<>();
    public Map<Integer, List<Integer>> submissionPerLibrary = new HashMap<>();
}