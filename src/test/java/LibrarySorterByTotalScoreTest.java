import org.junit.jupiter.api.Test;

import java.util.*;

import static java.util.stream.Collectors.joining;
import static org.junit.jupiter.api.Assertions.*;

class LibrarySorterByTotalScoreTest {

    @Test
    void sortLibraryByTotalScore() {
        int[] scores = new int[]{1, 2, 3, 4};
        Map<Integer, Set<Integer>> books = new TreeMap<>();
        books.put(0, new HashSet<>(Arrays.asList(0, 1, 3))); // 7
        books.put(1, new HashSet<>(Arrays.asList(2, 1, 3))); // 9 last
        books.put(2, new HashSet<>(Arrays.asList(0, 1, 2))); // 6 first
        books.put(3, new HashSet<>(Arrays.asList(0, 2, 3))); // 8

        books = LibrarySorterByTotalScore.sortLibraryByTotalScore(scores, books);
        assertEquals("2031", books.keySet().stream().map(String::valueOf).collect(joining()));
    }

    @Test
    void sum() {
        assertEquals(0, LibrarySorterByTotalScore.sum(new int[0], Collections.emptySet()));
        assertEquals(7, LibrarySorterByTotalScore.sum(new int[]{1, 2, 3, 4}, new HashSet<>(Arrays.asList(0, 1, 3))));
    }
}